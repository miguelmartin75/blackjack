#ifndef DECK_HPP
#define DECK_HPP

#include <utility>
#include <algorithm>
#include <array>

#include "Card.hpp"
#include "Constants.hpp"

class Deck
{
public:

    Deck();

    template <typename URNG>
    void shuffle(URNG&& g)
    {
        m_nextCard = 0;
        std::shuffle(m_cards.begin(), m_cards.end(), std::forward<URNG>(g));
    }

    Card next();
    bool empty() const;

private:

    std::array<Card, AMOUNT_OF_CARDS_IN_A_DECK> m_cards;
    size_t m_nextCard;
};


#endif // DECK_HPP
