#ifndef PLAYERDATA_HPP
#define PLAYERDATA_HPP

#include <ostream>

#include "Money.hpp"

struct PlayerData
{
    PlayerData(unsigned round, Money money) : round(round), moneyAtEndOfRound(money) {}
    unsigned round; 
    Money moneyAtEndOfRound;
};

std::ostream& operator<<(std::ostream& out, const PlayerData& data);

#endif // PLAYERDATA_HPP
