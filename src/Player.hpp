#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <vector>

#include "Hand.hpp"
#include "Card.hpp"
#include "Move.hpp"

struct Player
{
public:

    typedef std::vector<Card> CardList;

    virtual ~Player() = 0;

    virtual Move nextMove() = 0;
    virtual void recieve(const Card& card);

    const Hand& currentHand() const { return m_currentHand; }
    void resetHand() { m_hasSoftHand = false; m_cardsInHand.clear(); m_currentHand = 0; }

    const CardList& cards() const { return m_cardsInHand; }
    const Card& firstCard() const { return m_cardsInHand[0]; }

    bool hasSoftHand() const { return m_hasSoftHand; }

private:

    CardList m_cardsInHand;
    Hand m_currentHand = 0;
    bool m_hasSoftHand = false;
};

#endif // PLAYER_HPP
