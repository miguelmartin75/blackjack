#include "Player.hpp"

Player::~Player()
{
}

void Player::recieve(const Card& card)
{
    static constexpr const Hand VALUES_FOR_CARDS[] =
    {
        11,  // ACE
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10, // 10
        10, // JACK
        10, // QUEEN
        10  // KING
    };

    m_cardsInHand.push_back(card);

    Hand newHand = m_currentHand + VALUES_FOR_CARDS[static_cast<size_t>(card.value)];

    if(newHand > 21 && card.value == Card::Value::ACE)
    {
        newHand -= 10;
        m_hasSoftHand = true;
    }

    m_currentHand = newHand;
}
