#include "Strategy.hpp"

Move Strategy::optimalMoveFor(const Card& dealerCard, const Hand& playerHand) const
{
    const size_t dealerIndex = static_cast<size_t>(dealerCard.value);
    const size_t playerIndex = static_cast<size_t>(playerHand - PLAYER_MIN_CARD_SCORE_IN_HAND);
    return m_moves[dealerIndex][playerIndex];
}

void Strategy::optimalMoveFor(const Card& dealerCard, const Hand& playerHand, Move move)
{
    const size_t dealerIndex = static_cast<size_t>(dealerCard.value);
    const size_t playerIndex = static_cast<size_t>(playerHand - PLAYER_MIN_CARD_SCORE_IN_HAND);
    m_moves[dealerIndex][playerIndex] = move;
}

#include <iostream>
std::istream& operator>>(std::istream& in, Strategy& s)
{
    // ignore first line, as it's useless
    std::string buffer;
    std::getline(in, buffer);

    // per line
    for (size_t i = 0; i < Strategy::PLAYER_MOVES_LENGTH; ++i)
    {
        Hand hand = static_cast<Hand>(i + Strategy::PLAYER_MIN_CARD_SCORE_IN_HAND);
        in >> buffer; // ignore crap
        
        for (size_t j = 0; j < Strategy::DEALER_CARDS_LENGTH; ++j)
        {
            Move move;
            Card dealerCard{static_cast<Card::Value>(j), Card::Suite::DIAMONDS};
            in >> move;
            s.optimalMoveFor(dealerCard, hand, move);
        }
    }
    
    return in;
}

std::ostream& operator<<(std::ostream& out, const Strategy& s)
{
    // TOP 
    out << '\t';
    for (size_t i = 0u; i < Strategy::DEALER_CARDS_LENGTH; ++i)
    {
        out << static_cast<Card::Value>(i) << ' ';
    }

    out << '\n';

    for (size_t i = 0; i < Strategy::PLAYER_MOVES_LENGTH; ++i)
    {
        Hand hand = static_cast<Hand>(i + Strategy::PLAYER_MIN_CARD_SCORE_IN_HAND);
        out << i + Strategy::PLAYER_MIN_CARD_SCORE_IN_HAND << '\t';
        for (size_t j = 0; j < Strategy::DEALER_CARDS_LENGTH; ++j)
        {
            Card dealerCard{static_cast<Card::Value>(j), Card::Suite::DIAMONDS};
            out << s.optimalMoveFor(dealerCard, hand) << ' ';
        }
        out << '\n';
    }

    return out;
}
