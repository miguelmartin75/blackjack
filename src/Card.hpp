#ifndef CARD_HPP
#define CARD_HPP

#include <ostream>

struct Card 
{
    enum class Value 
    {
        ACE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        TEN,
        JACK,
        QUEEN,
        KING,

        COUNT
    } value;

    enum class Suite
    {
        SPADES,
        HEARTS,
        DIAMONDS,
        CLUBS,

        COUNT
    } suite;

    friend std::ostream& operator<<(std::ostream& os, const Card card);
    friend std::ostream& operator<<(std::ostream& os, const Card::Value value);
    friend std::ostream& operator<<(std::ostream& os, const Card::Suite suite);
};

#endif // CARD_HPP
