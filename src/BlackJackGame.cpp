#include "BlackJackGame.hpp"

#include <iostream>
#include <cstdlib>

#include "LoadSave.hpp"

static constexpr const unsigned int AMOUNT_OF_TIMES_TO_PLAY = 100000;

std::ostream& operator<<(std::ostream& out, const BlackJackGame::PlayingPlayer& player)
{
    out << "Round,Money\n";
    const auto AMOUNT_OF_ROUNDS = player.game().config().amountOfRoundsToPlay;
    for(int i = 0; i < AMOUNT_OF_ROUNDS; ++i)
    {
        Money averageForRound = 0;
        for(auto& data : player.data)
        {
            averageForRound += data[i].moneyAtEndOfRound;
        }
        averageForRound /= AMOUNT_OF_TIMES_TO_PLAY;
        
        out << i + 1 << "," << averageForRound << '\n';
    }
    return out;
}

BlackJackGame::BlackJackGame() :
    m_randomEngine{m_randomDevice()},
    m_dealer{m_randomEngine}
{
    m_dealer.onShuffle([this](){ onShuffle(); });
}

void BlackJackGame::init()
{
    std::cout << "Initializing game\n";
    m_dealer.amountOfDecks(config().amountOfDecks);

    if(config().isUsingStrategy)
    {
        if(!load(m_strategy, config().stratergyFile))
        {
            std::cerr << "Failed to load strategy file\n";
            std::exit(-1);
        }
        std::cout << "Loaded strategy:\n";
        std::cout << m_strategy << '\n';

        if(!load(m_softHand, config().softHandStrategyFile))
        {
            std::cerr << "Failed to load strategy file\n";
            std::exit(-4);
        }
        std::cout << "Loaded soft strategy:\n";
        std::cout << m_softHand << '\n';
    }

    if(config().isCardCounting)
    {
        if(!load(m_cardCountSystem, config().cardCountingFile))
        {
            std::cerr << "Failed to load card count system\n";
            std::exit(-2);
        }
        std::cout << "Loaded card count system:\n";
        std::cout << m_cardCountSystem << '\n';
    }

    for (unsigned i = 0u; i < config().amountOfPlayers; ++i)
    {
        PlayingPlayer player(*this);
        player.money(config().initialMoney);
        player.baseBet(config().baseBet);

        if(config().isUsingStrategy) 
        {
            player.strategy(&m_strategy);   
            player.softStrategy(&m_softHand);
        }
        if(config().isCardCounting) { player.countSystem(&m_cardCountSystem); }

        m_players.push_back(player);
    }

    for(unsigned i = 0; i < AMOUNT_OF_TIMES_TO_PLAY; ++i)
    {
        for(auto& player : m_players)
        {
            player.data.emplace_back();
            player.data[i].emplace_back(0, player.money());
        }
    }
}

void BlackJackGame::run()
{
    init();
    
    for(unsigned j = 0; j < AMOUNT_OF_TIMES_TO_PLAY; ++j)
    {
        for(auto& player : m_players)
        {
            player.resetCount();
            player.resetHand();
            player.money(config().initialMoney);
        }
        
        for (unsigned i = 1; i <= m_config.amountOfRoundsToPlay; ++i)
        {
            initRound();
            playRound();
            
            // update the data for the round
            for(auto& player : m_players)
            {
                player.data[j].emplace_back(i, player.money());
            }
        }
    }
    
    std::cout << "played " << m_config.amountOfRoundsToPlay << " rounds ";
    std::cout << AMOUNT_OF_TIMES_TO_PLAY << " times\n";

    std::ofstream listOfPlayerNames{"out/player_list.txt"};
    if(!listOfPlayerNames.is_open())
    {
        std::cerr << "failed to load list of player name file\n";
        std::exit(-4);
    }

    for (size_t i = 0u; i < m_players.size(); ++i)
    {
        auto playerName = "player" + std::to_string(i + 1);
        listOfPlayerNames << playerName << '\n';
        if(!save(m_players[i], "out/" + playerName + ".csv"))
        {
            std::cerr << "Failed to save player " << i + 1 << " data\n";
            std::exit(-3);
        }
        std::cout << "Saved player " << i + 1 << " data\n";
    }
}

void BlackJackGame::initRound()
{
    // reset everything prior to playing
    m_playingPlayers.clear();

    // reset hands
    {
        m_dealer.resetHand();

        // let the players decide whether to bet within
        // the game or not first
        for(auto& p : m_players)
        {
            p.resetHand();
            
            if(p.wasInGame && p.isInGame())
            {
                m_playingPlayers.push_back(&p);
                p.decideInitialBet();
                p.wasInGame = true;
            }
            else
            {
                p.wasInGame = false;
            }
        }
    }

    // check if we need to shuffle
    // as automatic shufflers can 
    // shuffle after every round
    if(config().automaticShuffler)
    {
        m_dealer.shuffle();
    }
}

void BlackJackGame::playRound()
{
    // deal cards to the dealer
    dealTo(m_dealer);

    std::vector<CardCountingPlayer*> playersToRemove;

    // deal to the players currently playing
    for(auto& player : m_playingPlayers)
    {
        dealTo(*player);
        
        if(player->currentHand() == 21 && player->cards().size() == 2)
        {
            player->money(player->money() - (3.f/2)*player->currentBet());
            playersToRemove.push_back(player);
            continue;
        }
    }
    
    for(auto& player : playersToRemove)
    {
        // remove the player from the playing players
        m_playingPlayers.erase(std::remove(m_playingPlayers.begin(), m_playingPlayers.end(), player), m_playingPlayers.end());
    }
    
    // main loop for a round
    
    bool dealerHasStood = false;
    std::vector<CardCountingPlayer*> stoodPlayers;

    // whilst there is players playing
    // or the dealer has not stood
    while(!m_playingPlayers.empty() || !dealerHasStood)
    {
        if(!dealerHasStood)
        {
            if(m_dealer.nextMove() == Move::STAND)
            {
                dealerHasStood = true;
            }
            else // assume a hit
            {
                hit(m_dealer);
            }
        }
        
        if(bust(m_dealer))
        {
            break;
        }
        
        playersToRemove.clear();
        for(auto& player : m_playingPlayers)
        {
            player->decideToChangeBet();

            // if we're bust we lost
            // NOTE: I'm doing it here to be more close to real life
            if(bust(*player))
            {
                player->money(player->money() - player->currentBet());
                playersToRemove.push_back(player);
                continue;
            }

            Move moveToMake = player->nextMove();
            if(moveToMake == Move::STAND)
            {
                stoodPlayers.push_back(player);
                playersToRemove.push_back(player);
            }
            else
            {
                hit(*player);
            }
        }

        for(auto& player : playersToRemove)
        {
            // remove the player from the playing players
            m_playingPlayers.erase(std::remove(m_playingPlayers.begin(), m_playingPlayers.end(), player), m_playingPlayers.end());
        }
    }

    // finally once the round is over
    // let the player see the cards of others
    for(auto& player : m_playingPlayers)
    {
        makeAllPlayersSeeExistingCards(*player);
        checkIfWon(*player);
    }

    for(auto& player : stoodPlayers)
    {
        makeAllPlayersSeeExistingCards(*player);
        checkIfWon(*player);
    }
}

void BlackJackGame::makeAllPlayersSee(const Player& player, const Card& card)
{
    for(auto& p : m_players)
    {
        if(&p == &player)
            continue;

        p.see(card);
    }
}

void BlackJackGame::makeAllPlayersSeeExistingCards(const Player& player)
{
   for (size_t i = 1; i < player.cards().size(); ++i)
   {
       makeAllPlayersSee(player, player.cards()[i]);
   }
}

void BlackJackGame::dealTo(Player& player)
{
    hit(player);
    hit(player);
}

void BlackJackGame::dealTo(CardCountingPlayer& player)
{
    hit(player);
    hit(player);
}

void BlackJackGame::hit(Player& player)
{
    auto card = m_dealer.deal();
    player.recieve(card);
}

void BlackJackGame::hit(CardCountingPlayer& player)
{
    auto card = m_dealer.deal();
    player.recieve(card);
    makeAllPlayersSee(player, card);
}

void BlackJackGame::checkIfWon(CardCountingPlayer& player)
{
    bool won = (bust(m_dealer) && !bust(player)) || (!bust(player) && player.currentHand() >= m_dealer.currentHand());
    Money moneyWon = 0;
    if(won)
    {
        moneyWon = player.currentBet();
    }
    else
    {
        moneyWon = -player.currentBet();
    }

    player.money(player.money() + moneyWon);
}

bool BlackJackGame::bust(Player& player) const
{
    return player.currentHand() > 21;
}

void BlackJackGame::onShuffle()
{
    // reset players counts
    for(auto& player : m_players)
    {
        player.resetCount();
    }
}

std::ostream& operator<<(std::ostream& out, const BlackJackGame::Config& config)
{
    out << "amountOfPlayers: " << config.amountOfPlayers << '\n';;
    out << "amountOfRoundsToPlay: " << config.amountOfRoundsToPlay << '\n';
    out << "amountOfDecks: " << config.amountOfDecks << '\n';
    out << "initialMoney: " << config.initialMoney << '\n';
    out << "baseBet: " << config.baseBet << '\n';
    out << "isCardCounting: " << config.isCardCounting << '\n';
    out << "isUsingStrategy: " << config.isUsingStrategy << '\n';
    out << "automaticShuffler: " << config.automaticShuffler << '\n';
    out << "cardCountingFile: " << config.cardCountingFile << '\n';
    out << "stratergyFile: " << config.stratergyFile << '\n';
    out << "softHandStrategyFile: " << config.softHandStrategyFile << '\n';

    return out;
}

std::istream& operator>>(std::istream& input, BlackJackGame::Config& config)
{
    input >> config.amountOfPlayers;
    input >> config.amountOfRoundsToPlay;
    input >> config.amountOfDecks;
    input >> config.initialMoney;
    input >> config.baseBet;
    input >> config.isCardCounting;
    input >> config.isUsingStrategy;
    input >> config.automaticShuffler;
    input.ignore();
    std::getline(input, config.cardCountingFile);
    std::getline(input, config.stratergyFile);
    std::getline(input, config.softHandStrategyFile);

    return input;
}
