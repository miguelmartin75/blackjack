#include "CardCountingSystem.hpp"

namespace 
{
    inline size_t indexFor(const Card::Value& value)
    {
        return static_cast<size_t>(value);
    }
}

CardCountingSystem::CardCountingSystem()
{
    m_spec.fill(0);
}

CardCountingSystem::CardCountingSystem(const Specification& spec)
{
    specification(spec);
}

void CardCountingSystem::count(const Card::Value& value, Count count)
{
    m_spec[indexFor(value)] = count;
}

Count CardCountingSystem::count(const Card::Value& value) const
{
    return m_spec[indexFor(value)];
}

void CardCountingSystem::specification(const Specification& spec)
{
    std::copy(spec.begin(), spec.end(), m_spec.begin());
}

const CardCountingSystem::Specification& CardCountingSystem::specification() const
{
    return m_spec;
}

std::ostream& operator<<(std::ostream& os, const CardCountingSystem& ccs)
{
    // print out card values
    for(size_t i = 0; i < AMOUNT_OF_CARDS_PER_SUITE; ++i)
    {
        Card::Value cardVal = static_cast<Card::Value>(i);
        os << cardVal << ' ';
    }
    
    os << '\n';

    // print out count values
    for(size_t i = 0; i < AMOUNT_OF_CARDS_PER_SUITE; ++i)
    {
        Card::Value cardVal = static_cast<Card::Value>(i);
        os << ccs.count(cardVal) << ' ';
    }
    
    return os;
}

std::istream& operator>>(std::istream& in, CardCountingSystem& ccs)
{
    std::string buffer;
    std::getline(in, buffer);

    for (size_t i = 0u; i < AMOUNT_OF_CARDS_PER_SUITE; ++i)
    {
        Card::Value cardVal = static_cast<Card::Value>(i);
        Count count;
        in >> count;

        ccs.count(cardVal, count);
    }

    return in;
}
