#include "Card.hpp"

std::ostream& operator<<(std::ostream& os, const Card::Value value)
{
    static constexpr const char* strings[] = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

    os << strings[static_cast<size_t>(value)];
    return os;
}

std::ostream& operator<<(std::ostream& os, const Card::Suite suite)
{
    static constexpr const char* strings = "SHDC";

    os << strings[static_cast<size_t>(suite)];
    return os;
}

std::ostream& operator<<(std::ostream& os, const Card card)
{
    os << card.value;
    os << card.suite;
    return os;
}
