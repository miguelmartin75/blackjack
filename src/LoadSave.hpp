#ifndef LOADSAVE_HPP
#define LOADSAVE_HPP

#include <string>
#include <fstream>

template <typename T>
bool load(T& object, const std::string& filepath)
{
    std::ifstream file{filepath};
    return file.is_open() && file >> object;
}

template <class T>
bool save(T& object, const std::string& filepath)
{
    std::ofstream file{filepath};
    return file.is_open() && file << object;
}

#endif // LOADSAVE_HPP
