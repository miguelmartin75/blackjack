#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include "Card.hpp"

constexpr const unsigned AMOUNT_OF_CARDS_PER_SUITE = static_cast<unsigned>(Card::Value::COUNT);
constexpr const unsigned AMOUNT_OF_SUITES = static_cast<unsigned>(Card::Suite::COUNT);
constexpr const unsigned AMOUNT_OF_CARDS_IN_A_DECK = AMOUNT_OF_CARDS_PER_SUITE * AMOUNT_OF_SUITES;

#endif // CONSTANTS_HPP