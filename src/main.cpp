#include <iostream>
#include <cstring>

#include "BlackJackGame.hpp"
#include "LoadSave.hpp"

enum class Error : int
{
    SUCCESS,
    NOT_ENOUGH_ARGS,
    FAILED_TO_LOAD_CONFIG_FILE,
    INVALID_COMMAND_LINE_FORMAT
};

std::ostream& operator<<(std::ostream& out, const Error& error)
{
    constexpr const char* STRINGS[] = { "Success", "Not Enough Args", "Failed to load config file", "Invalid command line format" };
    out << STRINGS[static_cast<int>(error)];
    return out;
}

// program MUST be run with a config file
// -c to override card counting file
// -s to override strategy file
Error processCommandArgs(BlackJackGame& game, int argc, char* argv[]);

void showHelp(const char* programName);

int main(int argc, char *argv[])
{
    BlackJackGame game;
    Error error;

    if((error = processCommandArgs(game, argc, argv)) != Error::SUCCESS)
    {
        std::cout << "Error: " << error << '\n';
        if(error == Error::INVALID_COMMAND_LINE_FORMAT || error == Error::NOT_ENOUGH_ARGS)
            showHelp(argv[0]);
        return static_cast<int>(error);
    }

    game.run();

    return 0;
}

void showHelp(const char* programName)
{
    std::cout << "syntax:\n";
    std::cout << "\t";
    std::cout << programName << " <X> [-c <Y>] [-s <Z>]\n";
    std::cout << "\t";
    std::cout << "where <X> is the file path to the config file\n";
    std::cout << "\t";
    std::cout << "where <Y> is a file path to a card count file\n";
    std::cout << "\t";
    std::cout << "where <Z> is a file path to a strategy file\n";
}

Error processCommandArgs(BlackJackGame& game, int argc, char* argv[])
{
    if(argc <= 1) return Error::NOT_ENOUGH_ARGS;
    
    std::string configFilePath = argv[1];
    std::cout << "Loading config: \"" << configFilePath << "\"\n";
    if(!load(game.config(), configFilePath))
    {
        return Error::FAILED_TO_LOAD_CONFIG_FILE;
    }
    std::cout << "Loaded config:\n";
    std::cout << game.config();
    
    if(argc == 2) goto success;

    for (int i = 2; i < argc; ++i)
    {
        if(argv[i][0] != '-' || std::strlen(argv[i]) != 2 || i + 1 == argc)
                return Error::INVALID_COMMAND_LINE_FORMAT;

        switch(argv[i][1])
        {
            case 'c':
                game.config().cardCountingFile = argv[i+1];
                break;
            case 's':
                game.config().stratergyFile = argv[i+1];
                break;
            default:
                return Error::INVALID_COMMAND_LINE_FORMAT;
        }
    }

success:
    return Error::SUCCESS;
}

