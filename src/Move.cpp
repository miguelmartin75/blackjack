#include "Move.hpp"

std::ostream& operator<<(std::ostream& out, const Move& move)
{
    static constexpr const char* MOVE_STRINGS = "HS";
    out << MOVE_STRINGS[static_cast<size_t>(move)];
    return out;
}


std::istream& operator>>(std::istream& in, Move& move)
{
    char c;
    in >> c;
    switch(c)
    {
        case 'H':
            move = Move::HIT;
            break;
        case 'S':
            move = Move::STAND;
            break;
        default:
            move = Move::HIT;
            break;
    }

    return in;
}
