#ifndef STRATEGY_HPP
#define STRATEGY_HPP

#include "Constants.hpp"

#include "Card.hpp"
#include "Move.hpp"
#include "Hand.hpp"

// A Strategy is just basically
// a grid of optimal plays
class Strategy
{
public:

    // don't include 21, as that's an instant win (duh)
    static constexpr const unsigned PLAYER_MIN_CARD_SCORE_IN_HAND = 4;
    static constexpr const unsigned PLAYER_MAX_CARD_SCORE_IN_HAND = 21;

    
    void optimalMoveFor(const Card& dealerCard, const Hand& playerHand, Move move);
    Move optimalMoveFor(const Card& dealerCard, const Hand& playerHand) const;

    friend std::istream& operator>>(std::istream&, Strategy&);
    friend std::ostream& operator<<(std::ostream&, const Strategy&);

private:

    static constexpr const unsigned DEALER_CARDS_LENGTH = AMOUNT_OF_CARDS_PER_SUITE;
    static constexpr const unsigned PLAYER_MOVES_LENGTH = PLAYER_MAX_CARD_SCORE_IN_HAND - PLAYER_MIN_CARD_SCORE_IN_HAND + 1;
 
    Move m_moves[DEALER_CARDS_LENGTH][PLAYER_MOVES_LENGTH] = {};
};

#endif // STRATEGY_HPP
