#ifndef MOVE_HPP
#define MOVE_HPP

#include <istream>
#include <ostream>

enum class Move
{
    HIT,
    STAND
};

std::ostream& operator<<(std::ostream&, const Move& move);
std::istream& operator>>(std::istream&, Move& move);

#endif // MOVE_HPP
