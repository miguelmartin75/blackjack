#include "CardCountingPlayer.hpp"

#include "BlackJackGame.hpp"

CardCountingPlayer::CardCountingPlayer(BlackJackGame& game, const Money& money) :
    m_game(game),
    m_runningCount(0),
    m_money(money),
    m_baseBet(0),
    m_strategy(nullptr),
    m_softStrategy(nullptr),
    m_countSystem(nullptr)
{
}

CardCountingPlayer::~CardCountingPlayer() {}

void CardCountingPlayer::see(const Card& card)
{
    if(!isCardCounting())
    {
        return; 
    }

    // increase the running count
    auto countForCard = m_countSystem->count(card.value);
    m_runningCount += countForCard;
}

void CardCountingPlayer::recieve(const Card& card)
{
    Player::recieve(card);
    see(card);
}

Move CardCountingPlayer::nextMove()
{
    if(!m_strategy)
    {
        // TODO: come up with a random move?
        return Move::HIT;
    }

    if(trueCount() >= 2 && !hasSoftHand())
    {
        return Move::STAND;
    }
    
    if(hasSoftHand() && m_softStrategy)
    {
        return m_softStrategy->optimalMoveFor(game().dealersCard(), currentHand());
    }

    return m_strategy->optimalMoveFor(game().dealersCard(), currentHand());
}

bool CardCountingPlayer::isInGame() const
{
    if(money() < m_baseBet) return false;
    return true;
}

void CardCountingPlayer::decideInitialBet()
{
    // TODO: Put this config in a file? 
    m_currentBet = 0;
    changeBet(m_baseBet);
}

void CardCountingPlayer::decideToChangeBet()
{
    // http://www.countingedge.com/truecount.html
    if(trueCount() <= 1)
    {
        return;
    }

    if(trueCount() <= 3)
    {
        changeBetByMultiplier(2);
    }
    else if(trueCount() <= 5)
    {
        changeBetByMultiplier(3);
    }
    else if(trueCount() <= 7)
    {
        changeBetByMultiplier(4);
    }
    else
    {
        changeBetByMultiplier(5);
    }
}

Count CardCountingPlayer::trueCount() const
{
    return runningCount() / game().decksLeft();
}

void CardCountingPlayer::changeBet(Money to)
{
    if(money() >= to && m_currentBet < to)
    {
        m_currentBet = to;
    }
}

void CardCountingPlayer::changeBetByMultiplier(float multplier)
{
    changeBet(static_cast<Money>(m_baseBet * multplier));
}
