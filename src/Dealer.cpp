#include "Dealer.hpp"

Dealer::Dealer(RandomEngine& engine) :
    m_randomEngine(engine),
    m_currentDeck(0)
{
}

void Dealer::amountOfDecks(size_t amount)
{
    m_decks.resize(amount);
    shuffle();
}

Move Dealer::nextMove()
{
    if(currentHand() == m_rules.hand)
    {
        if(m_rules.hitsOnSoftHand && hasSoftHand())
        {
            return Move::HIT;
        }
        else if(hasSoftHand())
        {
            return Move::STAND;
        }
    }
    
    // otherwise do move on hard hands
    if(currentHand() >= m_rules.hand)
    {
        return m_rules.move;
    }

    return Move::HIT;
}

void Dealer::shuffle()
{
    m_currentDeck = 0;
    for(auto& deck : m_decks)
    {
        deck.shuffle(m_randomEngine);
    }
    m_onShuffle();
}

Card Dealer::deal()
{
    if(currentDeck().empty())
    {
        ++m_currentDeck;

        if(m_currentDeck >= amountOfDecks())
        {
            shuffle();
        }
    }

    return currentDeck().next();
}
