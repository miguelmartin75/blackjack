#ifndef CARDCOUNTINGPLAYER_HPP
#define CARDCOUNTINGPLAYER_HPP

#include "Player.hpp"

#include "Money.hpp"
#include "Count.hpp"
#include "CardCountingSystem.hpp"
#include "Strategy.hpp"

class BlackJackGame;
class CardCountingPlayer : public Player
{
public:

    static constexpr const Count MIN_COUNT_THRESHOLD = -7;

    CardCountingPlayer(BlackJackGame& game, const Money& money = Money());
    CardCountingPlayer(const CardCountingPlayer&) = default;

    virtual ~CardCountingPlayer();

    virtual Move nextMove() override;
    virtual void recieve(const Card& card) override;

    void see(const Card& card);

    bool isInGame() const;

    // this may or may not change the bet
    void decideInitialBet();
    void decideToChangeBet(); // NOTE THIS CANNOT DECREASE THE BET
    Money currentBet() const { return m_currentBet; }

    BlackJackGame& game() const { return m_game; }

    void money(const Money& money) { m_money = money; }
    const Money& money() const { return m_money; }

    void strategy(const Strategy* strategy) { m_strategy = strategy; }
    const Strategy* strategy() const { return m_strategy; }

    void softStrategy(const Strategy* strategy) { m_softStrategy = strategy; }
    const Strategy* softStrategy() const { return m_softStrategy; }

    void countSystem(const CardCountingSystem* system) { m_countSystem = system; }
    const CardCountingSystem* countSystem() const { return m_countSystem; }

    const Money& baseBet() const { return m_baseBet; }
    void baseBet(const Money& baseBet) { m_baseBet = baseBet; }

    void resetCount() { m_runningCount = 0; }
    Count runningCount() const { return m_runningCount; }
    Count trueCount() const;
    
    bool isCardCounting() const { return m_countSystem != nullptr; }

private:

    void changeBet(Money to);
    void changeBetByMultiplier(float multplier);

    BlackJackGame& m_game;
    Count m_runningCount;
    Money m_money;
    Money m_currentBet;
    Money m_baseBet;
    const Strategy* m_strategy;
    const Strategy* m_softStrategy;
    const CardCountingSystem* m_countSystem;
};

#endif // CARDCOUNTINGPLAYER_HPP
