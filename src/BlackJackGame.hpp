#ifndef BLACKJACKGAME_HPP
#define BLACKJACKGAME_HPP

#include <istream>
#include <ostream>

#include "Random.hpp"
#include "Player.hpp"
#include "PlayerData.hpp"
#include "Dealer.hpp"
#include "CardCountingPlayer.hpp"
#include "Strategy.hpp"
#include "CardCountingSystem.hpp"

class BlackJackGame
{
public:

    struct Config
    {
        unsigned amountOfPlayers = 1;
        unsigned amountOfRoundsToPlay = 10;
        unsigned amountOfDecks = 4;
        unsigned initialMoney = 1000;
        unsigned baseBet = 100;
        bool isCardCounting = true;
        bool isUsingStrategy = true;
        bool automaticShuffler = false;

        std::string cardCountingFile = "cardCount.cc";
        std::string stratergyFile = "strategy.bs";
        std::string softHandStrategyFile = "strategy_soft.bs";

        friend std::ostream& operator<<(std::ostream&, const Config&);
        friend std::istream& operator>>(std::istream&, Config& config);
    };

    BlackJackGame();

    void run();

    Config& config() { return m_config; }
    const Config& config() const { return m_config; }

    int decksLeft() const { return m_dealer.decksLeft(); }
    const Card& dealersCard() const { return m_dealer.firstCard(); }

private:

    void init();
    void playRound();
    void initRound();
    void makeAllPlayersSee(const Player& player, const Card& card);
    void makeAllPlayersSeeExistingCards(const Player& player);
    void hit(Player& player);
    void hit(CardCountingPlayer& player);
    void dealTo(Player& player);
    void dealTo(CardCountingPlayer& player);
    void checkIfWon(CardCountingPlayer& player);
    void onShuffle();
    bool bust(Player& player) const;

    struct PlayingPlayer : CardCountingPlayer
    {
        typedef std::vector<PlayerData> PlayerDataArray;
        PlayingPlayer(BlackJackGame& game) : CardCountingPlayer(game) {}
        std::vector<PlayerDataArray> data;
        bool wasInGame = true;
    };
    friend std::ostream& operator<<(std::ostream& out, const PlayingPlayer& player);

    std::vector<PlayingPlayer> m_players;
    std::vector<CardCountingPlayer*> m_playingPlayers;

    std::random_device m_randomDevice;
    RandomEngine m_randomEngine;

    Dealer m_dealer;
    Config m_config;

    // TODO: different strategies and ccs's for players?
    CardCountingSystem m_cardCountSystem; 
    Strategy m_strategy;
    Strategy m_softHand;
};

#endif // BLACKJACKGAME_HPP
