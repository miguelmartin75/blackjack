#ifndef ASSERT_HPP
#define ASSERT_HPP

#include <cassert>

#define ASSERT(condition, message) assert(condition && message) 

#endif // ASSERT_HPP
