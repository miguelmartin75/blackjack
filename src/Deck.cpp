#include "Deck.hpp"

#include "assert.hpp"

Deck::Deck() :
    m_nextCard(0)
{
    unsigned counter = 0u;
    for (unsigned s = 0u; s < AMOUNT_OF_SUITES; ++s)
    {
        Card::Suite suite = static_cast<Card::Suite>(s);
        for (size_t v = 0u; v < AMOUNT_OF_CARDS_PER_SUITE; ++v)
        {
            Card::Value value = static_cast<Card::Value>(v);
            m_cards[counter] = Card{value, suite};
            ++counter;
        }
    }
}

Card Deck::next()
{
    ASSERT(!empty(), "Deck is empty");
    return m_cards[m_nextCard++];
}

bool Deck::empty() const
{
    return m_nextCard >= AMOUNT_OF_CARDS_IN_A_DECK;
}
