#ifndef CARDCOUNTINGSYSTEM_HPP
#define CARDCOUNTINGSYSTEM_HPP

#include <ostream>
#include <istream>
#include <array>

#include "Count.hpp"
#include "Card.hpp"
#include "Constants.hpp"

class CardCountingSystem
{
public:
    
    using Specification = std::array<Count, AMOUNT_OF_CARDS_PER_SUITE>;

    CardCountingSystem();
    CardCountingSystem(const Specification& spec);

    void count(const Card::Value& card, Count count);
    Count count(const Card::Value& card) const;

    void specification(const Specification& spec);
    const Specification& specification() const;


    friend std::ostream& operator<<(std::ostream& os, const CardCountingSystem& ccs);
    friend std::istream& operator>>(std::istream& is, CardCountingSystem& ccs);

private:

    Specification m_spec;
};

#endif // CARDCOUNTINGSYSTEM_HPP
