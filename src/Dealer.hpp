#ifndef DEALER_HPP
#define DEALER_HPP

#include <vector>

#include "Random.hpp"
#include "Player.hpp"
#include "Hand.hpp"
#include "Deck.hpp"

using DeckList = std::vector<Deck>;

class Dealer : public Player
{
public:

    struct Rules
    {
        // hit or stand on Hand...
        Hand hand = 17;
        Move move = Move::STAND;
        bool hitsOnSoftHand = false;
    };

    Dealer(RandomEngine& engine);

    void rules(const Rules& rules) { m_rules = rules; }
    const Rules& rules() const { return m_rules; }
    size_t amountOfDecks() const { return m_decks.size(); }
    void amountOfDecks(size_t amount);

    virtual Move nextMove() override;

    Card deal();

    void shuffle();

    Deck& currentDeck() { return m_decks[m_currentDeck]; }
    const Deck& currentDeck() const { return m_decks[m_currentDeck]; }

    // TODO: use this for true count
    // NOTE: Not adding one as this will cause a division by zero
    int decksLeft() const { return static_cast<int>(m_decks.size() - m_currentDeck); }

    void onShuffle(std::function<void()> f) { m_onShuffle = f; }

private:

    std::function<void()> m_onShuffle;
    RandomEngine m_randomEngine;
    Rules m_rules;
    DeckList m_decks;
    size_t m_currentDeck;
};

#endif // DEALER_HPP
