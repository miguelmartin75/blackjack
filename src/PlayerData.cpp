#include "PlayerData.hpp"

std::ostream& operator<<(std::ostream& out, const PlayerData& data)
{
    out << data.round << "," << data.moneyAtEndOfRound;
    return out;
}
