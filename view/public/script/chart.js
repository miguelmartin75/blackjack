var listOfFiles = 'data/player_list.txt';
$(document).ready(function() {

    console.log('creating ui...');
    refreshUI();
    
    var refreshButton = document.getElementById('refresh_button');
    refreshButton.onclick = refreshUI;
});

function refreshUI() {
    var data = document.getElementById('dataList');
    // clear the current innerHTML, just in case
    data.innerHTML = "";

    var dataList = readTextFile('http://localhost:3000/' + listOfFiles).split('\n');
    for (var i = 0, len = dataList.length; i < len; i++) {
        if(dataList[i].length === 0) continue;
        var anchorNode = document.createElement('a');
        anchorNode.href = "#";
        anchorNode.className = "list-group-item";

        var textNode = document.createTextNode(dataList[i]);

        anchorNode.appendChild(textNode);
        data.appendChild(anchorNode);

        // add an event listener to change the chart
        anchorNode.onclick = changedChart;

        // we'll just click the first one by default
        if(i === 0) anchorNode.click();
    }
}

function changedChart(event) {

    var dataName = event.target.innerHTML;

    console.log('changed chart to: ' + dataName);

    // create our chart
    // to display the data
    var chart = c3.generate({
        bindto: '#chart',
        data: {
            x: 'Round',
            y: 'Money',
            url: 'http://localhost:3000/data/' + dataName + '.csv'
        },

        subchart: {
            show: true
        },
        zoom: {
            enabled: true
        }
    });

    // update the title
    document.getElementById('chartTitle').innerHTML = dataName;
}

function readTextFile(file) {
    var allText;
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function () {
        if(rawFile.readyState === 4) {
            if(rawFile.status === 200 || rawFile.status == 0) {
                allText = rawFile.responseText;
            }
        }
    }
    rawFile.send(null);
    return allText;
}
